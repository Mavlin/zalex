<?php
namespace Models;

class Currency extends Model{

    public function __construct($request = null){
        parent::__construct($request);
    }

    // парсит данные для вставки
    public function parseData( $name, $value ){
        return $this->connect->parse("(?s,?s,?s)", $name, $value);
    }

    // вставка
    public function addCurrency( $ins ){
        $result = false;
        if (!empty($ins)){
            $result = $this->connect->query("INSERT INTO tbl_Currency SET ?u ON DUPLICATE KEY UPDATE ?u", $ins, $ins);
        }
        return $result;
    }

    // получим список валют
    public function getList( $countOb=100 ){
        $sql = "select * from tbl_Currency order by cur_name limit ?i";
        return $this->connect->getAll( $sql, $countOb );
    }

    // remove item
    public function remove( $id ){
        $sql = "DELETE FROM tbl_currency WHERE  cur_id=?i";
        return $this->connect->query( $sql, $id );
    }

    // получим срез валют
    public function getSlice($date){
        $sql = "SELECT * FROM tbl_Currency WHERE cur_date IN (SELECT MAX(cur_date) FROM tbl_Currency where
 cur_date<=?s GROUP BY cur_name)";
        return $this->connect->getAll( $sql, $date );
    }

}

