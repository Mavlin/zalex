<?php
namespace Models;
class Model
{
    public $config;
    public $connect;

    public function __construct( $db_cnf, $request = null ){
        $this->config = $db_cnf;
        $this->connect = $this->connect();
    }

    private function connect(){
        return new SafeMySQL( $this->config );
    }

    /**
     * проверка наличия колонки в таблице
     *
     * @param $db string
     * @param $tbl string
     * @param $fld string
     * @return bool
     *
     * */
    public function isColumnExist( $db, $tbl, $fld ){
        $sql = "SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema=?s and 
TABLE_NAME=?s and Column_Name=?s";
        return $this->connect->getOne($sql, $db, $tbl, $fld);
    }
}
