<?php

namespace Controllers;

class Currency extends Controller //
{
    const
        CURRENCY_NAME_MAX_LENGTH = 15;
    protected $Currency; // ссылка на модель

    public function __construct( $req ){
        parent::setCfg();
        $this->Currency = new \Models\Currency( $this->config );
        if ($req !== null) {
            parent::__construct( $req );
        }
    }

    private function checkName($field){
        if ( iconv_strlen($field, 'utf-8') > $this::CURRENCY_NAME_MAX_LENGTH){
            $rc = $this->dataNotValid;
            throw new \Exception ($rc['message'], $rc['code']);
        }
    }

    public function save(){
        sleep (0);
        $this->decodeRequest($this->request[(string)__FUNCTION__]['data']);
        $ins = array();
        $ins['cur_name'] = $this->getRequestParam( $this->request, 'cur_name');
        $ins['cur_value']= $this->getRequestParam( $this->request, 'value');
        $ins['cur_date']= $this->getRequestParam( $this->request, 'date');
// http://phpfaq.ru/safemysql/examples#multiinsert

        $this->checkName($ins['cur_name']);
        $result = false;
        if (!empty($ins)){ // добавляем данные
            $result = $this->Currency->addCurrency( $ins );
        }
        if ($result) {
            $rc = $this->dataIsSaved(implode('; ',$ins));

        } else {
            $rc = $this->SomethingWrong;
        }
        $this->setResponse('data', $rc['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $rc['code']);
    }

    public function set(){
        $this->decodeRequest($this->request[(string)__FUNCTION__]['data']);
        $ins = array();
        $ins['cur_id'] = $this->getRequestParam( $this->request, 'cur_id');
        $ins['cur_name'] = $this->getRequestParam( $this->request, 'cur_name');
        $ins['cur_value']= $this->getRequestParam( $this->request, 'value');
        $ins['cur_date']= $this->getRequestParam( $this->request, 'date');
// http://phpfaq.ru/safemysql/examples#multiinsert

        $this->checkName($ins['cur_name']);

        $result = false;
        if (!empty($ins)){ // добавляем данные
            $result = $this->Currency->addCurrency( $ins );
        }
        if ($result) {
            $rc = $this->dataIsSaved(implode('; ',$ins));

        } else {
            $rc = $this->SomethingWrong;
        }
        $this->setResponse('data', $rc['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $rc['code']);
    }


    public function getSlice(){
// http://streletzcoder.ru/slozhnaya-vyiborka-poslednih-dannyih-po-date-iz-odnoy-tablitsyi-na-primere-microsoft-sql-server/
        $this->decodeRequest($this->request[(string)__FUNCTION__]['data']);
        $date = $this->getRequestParam( $this->request, 'date');
        $result = [];
        if (!empty($date)){ //
            $result = $this->Currency->getSlice($date);
        }
        if ($result) {
            $responseCode = $this->success;
            $this->setResponse('data', $result);
        } else {
            $responseCode = $this->SomethingWrong;
        }
        $this->setResponse('message', $responseCode['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $responseCode['code']);
    }

    public function remove(){
        $this->decodeRequest($this->request[(string)__FUNCTION__]['data']);
        $id = $this->getRequestParam( $this->request, 'cur_id');
        $result = [];
        if (!empty($id)){ //
            $result = $this->Currency->remove($id);
        }
        if ($result) {
            $responseCode = $this->success;
            $this->setResponse('data', $result);
        } else {
            $responseCode = $this->SomethingWrong;
        }
        $this->setResponse('message', $responseCode['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $responseCode['code']);
    }

}
