/**
 * возвращает ссылку на элемент
 *
 * @param {string} a
 * @return {Element}
 */
function gid( a ){
    return document.getElementById( a )
}

function setHeight( arg, height ){
    let el;
    if ( arg instanceof HTMLElement ){
        el = arg;
    } else {
        el = document.querySelector( arg );
    }
    el.style.height  = height + 'px'
}


/**
 * объект для работы с формами
 *
 * @param {string || HTMLElement} el
 * @set {this}
 * */
let Form = function ( el ) {
    // debugger
    if ( el instanceof HTMLElement ){
        this.form = el;
    } else {
        this.form = gid( el );
    }
    this.valid = 0;
    this.submitButt = this.form.querySelector('button[type="submit"]');
    this.inputs = this.form.querySelectorAll('input');
    this.inputsAll = this.form.querySelectorAll('input, textarea, button, select');
    this.data = {};
    this.name = this.form['name'];
};
/**
 * формирование массива данных с формы
 *
 * @this Form
 * @set {object} this.data
 * */
Form.prototype.getData = function (){
    let data = {};
    let arr = this.inputs, item;
    for ( let i = 0; i < arr.length; ++i ) {
        item = arr[ i ];
        //console.log ( item.dataset.name + ' - ' +  + ' == ' + item.innerHTML );
        if ((item.name || item.dataset.name) && item.innerHTML) {
            switch (item.tagName) { // для DOM элементов
                case 'P':
                case 'DIV':
                case 'SPAN':
                    data[item.dataset.name] = item.innerHTML;
                    break;
                case 'SELECT':
                    data[item.name || item.dataset.name] = item.value;
                    break;
                case 'UL':
                    /**
                     * чтобы использовать список, нужно ему присвоить класс для отправки данных
                     * датасет name, а элементам списка датасет id
                     * */
                        //console.log ( item.tagName +' ---- '+ item.innerHTML );
                        //console.log ( item.name + ' --- ' + item.value );
                        //console.log ( item.innerHTML );
                    let lis = item.childNodes, ar = [];
                    for (let i = 0; i < lis.length; ++i) {
                        let id = lis[i].dataset.id || lis[i].dataset.name;
                        if (lis.hasOwnProperty(i) && id) ar = ar.concat(id)
                    }
                    if (lis.length) data[item.dataset.name] = ar;
            }
        }
        if (item.type === 'checkbox' && item.checked) data[item.name] = 1;
        if (item.type === 'checkbox' && item.checked === false) data[item.name] = 0;
        if (item.type === 'radio' && item.checked) data[item.name] = item.value;
        if (item.type === 'date') data[item.name] = item.value;
        if (item.type === 'number') data[item.name] = item.value;
        switch (item.type) {
            case 'text':
            case 'password':
            case 'email':
            case 'textarea':
                if (item.dataset.type === 'number') data[item.name] = item.value.replace(/\s+/g, '');
                else if (item.value) data[item.name] = item.value;
                break;
            case 'file':
                break;
        }
    }
    this.data = data;
    return this.data;
};

function getDate(arg) {
    let date;
    if (arg){
        date = new Date(arg.substr(0,10));
    } else {
        date = new Date();
    }
    let month = 1+Number(date.getMonth()),
        day = date.getDate()
    if (month<10) month = '0' + month
    if (day<10) day = '0' + day
    return date.getFullYear()+'-'+month+'-'+day; // среда, 31 декабря 2014 г. н.э. 12:30:00
}

function Fetch( arg ){
    // console.log(arg)
    this.string = arg.string || null;
    this.method = arg.method;
    this.body = arg.body;
    this.credentials = arg.credentials || 'include'; // отослать куки - пока требуется только в отладке
    this.url = ()=>{
        return this.method.toLowerCase() === 'get' ? arg.url + arg.string : arg.url;
    };
    this.run =
        ()=>{
            return new Promise((resolve, reject)=>{
                    fetch(this.url(), {
                        method: this.method,
                        body: this.body,
                        credentials: this.credentials,
                    })
                        .then(response => {
                            return response.json();
                        })
                        .then(data => {
                            if (data.xdebug_message){
                                console.warn(data.data + ' in file: ' + data.file + ' in line: ' + data.line)
                                // document.body.insertAdjacentHTML('afterBegin', data.xdebug_message)
                                return
                            }
                            resolve(data)
                        })
                        .catch(err => {
                            reject(err);
                        });
                }
            )
        }
}

export { gid, Form, getDate, Fetch, setHeight }