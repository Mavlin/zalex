-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.18 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              9.5.0.5273
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных zalex
CREATE DATABASE IF NOT EXISTS `zalex` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `zalex`;

-- Дамп структуры для таблица zalex.tbl_currency
CREATE TABLE IF NOT EXISTS `tbl_currency` (
  `cur_id` int(11) NOT NULL AUTO_INCREMENT,
  `cur_name` text NOT NULL,
  `cur_value` int(11) NOT NULL DEFAULT '0',
  `cur_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cur_id`),
  UNIQUE KEY `Uniq` (`cur_name`(100),`cur_date`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы zalex.tbl_currency: ~9 rows (приблизительно)
/*!40000 ALTER TABLE `tbl_currency` DISABLE KEYS */;
REPLACE INTO `tbl_currency` (`cur_id`, `cur_name`, `cur_value`, `cur_date`) VALUES
	(216, 'доллар', 500, '2018-10-31 00:00:00'),
	(217, 'евро', 300, '2018-10-31 00:00:00'),
	(218, 'гривна', 250, '2018-10-31 00:00:00'),
	(219, 'юань', 700, '2018-10-31 00:00:00'),
	(220, 'лев', 800, '2018-10-31 00:00:00'),
	(224, 'шекель', 150, '2018-10-31 00:00:00'),
	(225, 'динар', 175, '2018-10-31 00:00:00'),
	(226, 'йена', 220, '2018-10-31 00:00:00'),
	(231, 'рубль', 1000, '2018-10-31 00:00:00');
/*!40000 ALTER TABLE `tbl_currency` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
