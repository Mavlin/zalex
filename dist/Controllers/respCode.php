<?php

namespace Controllers;
/* содержит коды ответов
 * */

trait respCode
{
    public $dataNotFound = [
        'code' => 3,
        'message' => 'Данных нет'
    ];
    public $success = [
        'code' => 0,
        'message' => 'Все идет по плану :-))'
    ];
    public $dataNotValid = [
        'code' => 2,
        'message' => 'данные некорректны(('
    ];
    public $purgeDB = [
        'code' => 5,
        'message' => 'таблица данных очищена'
    ];
    public function dataIsSaved( $data )
    {
        return [
            'code' => 12, //
            'message' => 'записаны данные: ' . $data
        ];
    }
    public $noField = [
        'code' => 24,
        'message' => "Требуемое поле не заполнено: "
    ];
    public $SomethingWrong = [
        'code' => 41,
        'message' => 'Something went wrong'
    ];
}
