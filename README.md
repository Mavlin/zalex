# Zalex

## Project setup
```
npm install
```
### Compiles and hot-reloads for development
```
npm run serve
```
### Compiles and minifies for production
```
npm run build
```
### Требования к среде разработки:
```
NodeJS, Vue-cli
```
Скомпилированный дистрибутив находится в папке dist;

Исходники в папке src и public.

Дамп БД в папке sqldamp;

Проверена работа в Гуглохроме последней на 2018-11 версии; 
